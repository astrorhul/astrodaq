import sqlite3
from epics import PV
import threading
import datetime
import time

class Database:
    def __init__(self):
        '''
        Connects to database, connects to input from LabVIEW, monitors
        LabVIEW variable 'Filepath' for change.
        When change occurs, commits all incoming data into a database
        '''
        try:
            self._conn = sqlite3.connect('\Users\domeuser\Desktop\Database\Astro_DB.sqlite')
            self._c = self._conn.cursor()
            self._notify = False
            self._changeSearch = False
        except:
            print "Error: Could not connect to database"
        tf = self.connectToInput()
        if tf == 1:
            self._dbOutput.put("Connected.")
        elif tf == 0:
            self._dbOutput.put("Error: Failed to connect to input")
            return
        self.addCallbacks()
        self.monitorLoopImageAndSearch()

    def createTable(self, table_name, field_name, field_type):
        '''Creates a table with one field'''
        self._c.execute('CREATE TABLE {tn} ({fn} {ft})'.format(tn = table_name, fn = field_name, ft = field_type))

    def addColumn(self,column_name, column_type):
        '''Add column to Images table'''
        self._c.execute("ALTER TABLE {tn} ADD COLUMN '{cn}' {ct}".format(tn = 'Images', cn = column_name, ct = column_type))

    def addDefaultTable(self):
        '''Creates a table with default columns'''
        self.createTable('Images', 'Filepath', 'TEXT')
        self.addColumn('Author', 'TEXT')
        self.addColumn('Date_DD/MM/YYYY', 'TEXT')
        self.addColumn('Time_HH:MM:SS', 'TEXT')
        self.addColumn('Centre_RA', 'REAL')
        self.addColumn('Centre_Dec', 'REAL')
        self.addColumn('Centre_Alt', 'REAL')
        self.addColumn('Centre_Az', 'REAL')
        self.addColumn('Scope_Used', 'TEXT')
        self.addColumn('Camera_Name', 'TEXT')
        self.addColumn('Camera_Bit_Depth', 'INTEGER')
        self.addColumn('Filter_Name', 'TEXT')
        self.addColumn('X_Binning', 'INTEGER')
        self.addColumn('Y_Binning', 'INTEGER')
        self.addColumn('Max_Brightness','INTEGER')
        self.addColumn('Exposure_Time', 'REAL')
        self.addColumn('X_Pixel_Width', 'INTEGER')
        self.addColumn('Y_Pixel_Width', 'INTEGER')
        self.addColumn('Total_Pixels', 'INTEGER')
        self.addColumn('Image_Type', 'TEXT')
        self.addColumn('Cooler_Temp', 'REAL')
        self.addColumn('Ambient_Temp', 'REAL')
        self.addColumn('Focus_Location', 'INTEGER')
        self.addColumn('Focal_Reducer', 'TEXT')

    def quit(self):
        '''Commit to database and close connection'''
        self._conn.commit()
        self._conn.close()
        self._dbOutput.put("Database connection terminated")

    def emptySharedVars(self):
        self._filepath.put(" ")
        self._author.put(" ")
        self._date.put(" ")
        self._time.put(" ")
        self._RA.put(0)
        self._Dec.put(0)
        self._alt.put(0)
        self._az.put(0)
        self._scope.put(" ")
        self._camName.put(" ")
        self._camBitDep.put(0)
        self._filterName.put(" ")
        self._xBin.put(0)
        self._yBin.put(0)
        self._maxBright.put(0)
        self._expTime.put(0)
        self._xPixWidth.put(0)
        self._yPixWidth.put(0)
        self._totalPix.put(0)
        self._imType.put(" ")
        self._coolerTemp.put(0)
        self._ambTemp.put(0)
        self._focusLoc.put(0)
        self._focusRed.put(" ")

    def combineSTL(self):
        self._i += 1
        qaa = self._STLInput.get().split('=')
        if (self._i < self._numberCombo.get()): #if this is not the last loop, include an 'AND'
            self._comboSTL = self._comboSTL + qaa[0] + '= ?' + ' AND '
        else:
            self._comboSTL += qaa[0] + '= ?'
        self.STLqs.append(qaa[1].lstrip())
        self._STLInput.put(" ")

    def stlSearch(self):
        '''Searches through db with STL input'''
        output = str(len(self.rowList)) + " search results found"
        self._dbOutput.put(output)
        while(self._searchBool.get() == 0):
            if(self._changeSearch == True):
                if self._searchIndex.get() >= len(self.rowList):
                    self.emptySharedVars()
                    self._dbOutput.put("Error: Search index out of range")
                    self._changeSearch = False
                    continue
                else:
                    self._dbOutput.put(output)
                    row = self.rowList[self._searchIndex.get()]
                    #put in variables
                    self._filepath.put(row[0])
                    self._author.put(row[1])
                    self._date.put(row[2])
                    self._time.put(row[3])
                    self._RA.put(row[4])
                    self._Dec.put(row[5])
                    self._alt.put(row[6])
                    self._az.put(row[7])
                    self._scope.put(row[8])
                    self._camName.put(row[9])
                    self._camBitDep.put(row[10])
                    self._filterName.put(row[11])
                    self._xBin.put(row[12])
                    self._yBin.put(row[13])
                    self._maxBright.put(row[14])
                    self._expTime.put(row[15])
                    self._xPixWidth.put(row[16])
                    self._yPixWidth.put(row[17])
                    self._totalPix.put(row[18])
                    self._imType.put(row[19])
                    self._coolerTemp.put(row[20])
                    self._ambTemp.put(row[21])
                    self._focusLoc.put(row[22])
                    self._focusRed.put(row[23])

                    self._changeSearch = False
                    continue
            else:
                continue
        return

    def searchAnyColumn(self):
        '''Searches through db with shared variables - used via monitorLoop'''
        output = str(len(self.rowList)) + " search results found"
        self._dbOutput.put(output)
        while(self._searchBool.get() == 0):
            if(self._changeSearch == True):
                if self._searchIndex.get() >= len(self.rowList):
                    self.emptySharedVars()
                    self._dbOutput.put("Error: Search index out of range")
                    self._changeSearch = False
                    continue
                else:
                    self._dbOutput.put(output)
                    row = self.rowList[self._searchIndex.get()]
                    #put in variables
                    self._filepath.put(row[0])
                    self._author.put(row[1])
                    self._date.put(row[2])
                    self._time.put(row[3])
                    self._RA.put(row[4])
                    self._Dec.put(row[5])
                    self._alt.put(row[6])
                    self._az.put(row[7])
                    self._scope.put(row[8])
                    self._camName.put(row[9])
                    self._camBitDep.put(row[10])
                    self._filterName.put(row[11])
                    self._xBin.put(row[12])
                    self._yBin.put(row[13])
                    self._maxBright.put(row[14])
                    self._expTime.put(row[15])
                    self._xPixWidth.put(row[16])
                    self._yPixWidth.put(row[17])
                    self._totalPix.put(row[18])
                    self._imType.put(row[19])
                    self._coolerTemp.put(row[20])
                    self._ambTemp.put(row[21])
                    self._focusLoc.put(row[22])
                    self._focusRed.put(row[23])

                    self._changeSearch = False
                    continue
            else:
                continue
        return

    def connectToInput(self):
        '''Connects to input from LabVIEW shared variables'''
        #search info
        self._searchCol = PV('phdome-pc1:ServerLibrary:Database_Search_Column', auto_monitor = True, connection_timeout = 10800)
        self._searchString = PV('phdome-pc1:ServerLibrary:Database_Search_Values', auto_monitor = True, connection_timeout = 10800)
        self._searchBool = PV('phdome-pc1:ServerLibrary:Database_Search_Complete', auto_monitor = True, connection_timeout = 10800)
        self._searchIndex = PV('phdome-pc1:ServerLibrary:Database_Search_Results_Index', auto_monitor = True, connection_timeout = 10800)
        self._STLInput = PV('phdome-pc1:ServerLibrary:Database_STL', auto_monitor = True, connection_timeout = 10800)
        self._numberCombo = PV('phdome-pc1:ServerLibrary:Database_Number_Conditions', auto_monitor = True, connection_timeout = 10800)
        self._STLSubmit = PV('phdome-pc1:ServerLibrary:Database_Enter_Condition', auto_monitor = True, connection_timeout = 10800)
        self._search = False
        self._STLSearchReady = False
        self._comboSTL = 'SELECT * FROM Images WHERE '
        self._i = 0
        self.STLqs = []

        #db info
        self._dbOutput = PV('phdome-pc1:ServerLibrary:Database_Output', auto_monitor = True, connection_timeout = 10800)
        self._dbOutput.put("Connecting to input...")
        self._dbSubmit = PV('phdome-pc1:ServerLibrary:Database_Submit', auto_monitor = True, connection_timeout = 10800)
        self._filepath = PV('phdome-pc1:ServerLibrary:Image_Filepath', auto_monitor = True, connection_timeout = 10800)
        self._author = PV('phdome-pc1:ServerLibrary:Image_Author', auto_monitor = True, connection_timeout = 10800)
        self._date = PV('phdome-pc1:ServerLibrary:Date', auto_monitor = True, connection_timeout = 10800)
        self._time = PV('phdome-pc1:ServerLibrary:Time', auto_monitor = True, connection_timeout = 10800)
        self._xBin = PV('phdome-pc1:ServerLibrary:Image_X_Binning', auto_monitor = True, connection_timeout = 10800)
        self._yBin = PV('phdome-pc1:ServerLibrary:Image_Y_Binning', auto_monitor = True, connection_timeout = 10800)
        self._imType = PV('phdome-pc1:ServerLibrary:Image_Image_Type', auto_monitor = True, connection_timeout = 10800)
        self._maxBright = PV('phdome-pc1:ServerLibrary:Image_Max_Brightness', auto_monitor = True, connection_timeout = 10800)
        self._totalPix = PV('phdome-pc1:ServerLibrary:Image_Total_Pixel_Number', auto_monitor = True, connection_timeout = 10800)
        self._xPixWidth = PV('phdome-pc1:ServerLibrary:Image_X_Pixel_Width', auto_monitor = True, connection_timeout = 10800)
        self._yPixWidth = PV('phdome-pc1:ServerLibrary:Image_Y_Pixel_Width', auto_monitor = True, connection_timeout = 10800)
        self._RA = PV('phdome-pc1:ServerLibrary:Telescope_RA', auto_monitor = True, connection_timeout = 10800)
        self._Dec = PV('phdome-pc1:ServerLibrary:Telescope_Dec', auto_monitor = True, connection_timeout = 10800)
        self._alt = PV('phdome-pc1:ServerLibrary:Telescope_Alt', auto_monitor = True, connection_timeout = 10800)
        self._az = PV('phdome-pc1:ServerLibrary:Telescope_Az', auto_monitor = True, connection_timeout = 10800)
        self._scope = PV('phdome-pc1:ServerLibrary:Telescope_Scope_Used', auto_monitor = True, connection_timeout = 10800)
        self._focusLoc = PV('phdome-pc1:ServerLibrary:Telescope_Focus_Location', auto_monitor = True, connection_timeout = 10800)
        self._focusRed = PV('phdome-pc1:ServerLibrary:Telescope_Focus_Reducer', auto_monitor = True, connection_timeout = 10800)
        self._ambTemp = PV('phdome-pc1:ServerLibrary:Temperature', auto_monitor = True, connection_timeout = 10800)
        self._camBitDep = PV('phdome-pc1:ServerLibrary:CCD_Camera_Bit_Depth', auto_monitor = True, connection_timeout = 10800)
        self._coolerTemp = PV('phdome-pc1:ServerLibrary:CCD_Cooler_Temp', auto_monitor = True, connection_timeout = 10800)
        self._camName = PV('phdome-pc1:ServerLibrary:CCD_Camera_Name', auto_monitor = True, connection_timeout = 10800)
        self._filterName = PV('phdome-pc1:ServerLibrary:CCD_Filter_Name', auto_monitor = True, connection_timeout = 10800)
        self._expTime = PV('phdome-pc1:ServerLibrary:CCD_Exposure_Time', auto_monitor = True, connection_timeout = 10800)

        if (self._filepath.connect()  and self._date.connect()   and self._yBin.connect() \
        and self._imType.connect()  and self._maxBright.connect()  and self._time.connect()  and self._xBin.connect()  \
        and self._xPixWidth.connect()  and self._yPixWidth.connect()  and self._totalPix.connect()  and self._RA.connect()   \
        and self._alt.connect()  and self._az.connect()  and self._scope.connect()  and self._focusLoc.connect()  \
        and self._ambTemp.connect()  and self._camBitDep.connect()  and self._coolerTemp.connect()  and self._camName.connect()  \
        and self._filterName.connect() and self._author.connect() and self._expTime.connect()  and self._Dec.connect() ):
            return 1
        else:
            return 0

    def addImage(self):
        '''Adds input to image database'''
        self.sql = "INSERT INTO Images VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
        self.task = [ self._filepath.get(), self._author.get(), self._date.get(), self._time.get(), self._RA.get(), self._Dec.get(), self._alt.get(), self._az.get(), \
        self._scope.get(), self._camName.get(), self._camBitDep.get(), self._filterName.get(), self._xBin.get(),\
        self._yBin.get(), self._maxBright.get(), self._expTime.get(), self._xPixWidth.get(), self._yPixWidth.get(), self._totalPix.get(), \
        self._imType.get(), self._coolerTemp.get(), self._ambTemp.get(), self._focusLoc.get(), self._focusRed.get() ]

    def addCallbacks(self):
        '''Adds callback to buttons and variables from LabVIEW'''
        self._dbSubmit.add_callback(self.cbSave)
        self._searchString.add_callback(self.cbSearch)
        self._searchIndex.add_callback(self.cbIndex)
        self._STLSubmit.add_callback(self.cbSTL)

    def cbSave(self, *a, **b):
        '''Saves new image when save to database 'Ok' button is pressed'''
        self.addImage()
        self._notify = True

    def cbSearch(self, *a, **b):
        '''Initiates search mode when search column is changed'''
        self._search = True

    def cbIndex(self, *a, **b):
        '''Adds callback to search result index -
        changes result being displayed'''
        self._changeSearch = True

    def cbSTL(self, *a, **b):
        '''Adds callback to STL submit condition'''
        self._STLSearchReady = True

    def monitorLoopImageAndSearch(self):
        '''Monitors whether the callback has been called.
        If it has, commits changes to db'''

        while True:
            try:
                time.sleep(1)
                if self._STLSearchReady:
                    self._dbOutput.put("Enter next condition")
                    try:
                        self.combineSTL()
                        self._STLSearchReady = False
                        if (self._i == self._numberCombo.get()): #if number of combo loops == total number of conditions, execute search
                            self._dbOutput.put("Searching...")
                            print [self._comboSTL]
                            print self.STLqs
                            self._c.execute(self._comboSTL, self.STLqs)
                            self.rowList = self._c.fetchall()
                            print self.rowList
                            self.stlSearch()
                            self._comboSTL = 'SELECT * FROM Images WHERE '
                            self._i = 0
                            self.emptySharedVars()
                            self._dbOutput.put("Search complete - ready for new data")
                            self.STLqs = []
                            self._searchIndex.put(0)
                            self._STLSearchReady = False
                            continue
                    except:
                        self._dbOutput.put("Error: Could not initiate search")
                        self._STLSearchReady = False
                        continue
                    continue
                if self._search:
                    try:
                        self._dbOutput.put("Searching...")
                        self._c.execute('SELECT * FROM Images WHERE {cn} = ?'.format(cn = self._searchCol.get()),[self._searchString.get()])
                        self.rowList = self._c.fetchall()
                        self.searchAnyColumn()

                        self._searchString.put(" ")
                        self.emptySharedVars()
                        self._dbOutput.put("Search complete - ready for new data")
                        self._searchIndex.put(0)
                        self._search = False
                        continue
                    except:
                        self._dbOutput.put("Error: Could not initiate search")
                        self._search = False
                        continue
                if self._notify:
                    try:
                        self._c.execute(self.sql, self.task)
                        self._conn.commit()
                        datetime.datetime.now().strftime("%H:%M:%S")
                        output = "Saved data at " + datetime.datetime.now().strftime("%H:%M:%S")
                        self._dbOutput.put(output)
                    except:
                        self._dbOutput.put("Error: Could not save data to database.")
                        continue
                self._notify  = False
            except (KeyboardInterrupt, SystemExit):
                print 'intterupt caught'
                self.quit()

#db = Database()
