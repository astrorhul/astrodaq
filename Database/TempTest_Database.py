import sqlite3
from epics import PV

class Temp:
    def createTempDB(self):
        self.c, self.conn  = self.connectTempDB()
        self.c.execute('CREATE TABLE Temperature (Temp REAL)')

    def connectTempDB(self):
        self.conn = sqlite3.connect('\Users\domeadmin\Desktop\Database\Temp.sqlite')
        self.c = self.conn.cursor()
        return self.c, self.conn

    def connectTempInput(self):
        '''connects to LabVIEW temperature intput'''
        self.temp = PV('phdome-pc1:ServerLibrary:Temperature', auto_monitor = True)
        return self.temp

    def addTemp(self, newtemp):
        '''Adds temp values to temp db'''
        self.c, self.conn  = self.connectTempDB()
        self.sql = "INSERT INTO Temperature (Temp) VALUES (?)"
        self.task = [newtemp]
        self.c.execute(self.sql,self.task)
        print newtemp
        self.conn.commit()

    def cbSave(self, *a, **b):
        '''callback function for auto_monitor'''
        self.addTemp(self.temp.get())

    def autoAddTemp(self):
        '''Automatically adds temperature input from LabVIEW to database'''
        self.temp = self.connectTempInput()
        self.temp.add_callback(self.cbSave)

#temp = Temp()
#temp.createTempDB()
#temp.autoAddTemp()
