﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="CCD_Camera_Bit_Depth" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!V!"A!'65FO&gt;$%W!!!"!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="CCD_Camera_Name" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="CCD_Cooler_Temp" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="CCD_Exposure_Time" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="CCD_Filter_Name" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Conditions" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!N!!A!&amp;37ZU-49!!1!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Output" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Search_Column" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Search_Complete" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Search_Results_Index" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!F)1!!!"=!A!!!!!!"!!V!"Q!'65FO&gt;$-S!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Search_Values" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_STL" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Database_Submit" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!"=!A!!!!!!"!!R!)1&gt;#&lt;W^M:7&amp;O!!%!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Date" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="EPICS Server1" Type="IO Server">
		<Property Name="atrb" Type="Str">"%E!!!Q1!!!C!!!!"B!!!!=!!!"3!'5!9Q"P!()!:!!Q!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!)1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"F!'U!=!"5!'E!&lt;1"F!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#1!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!&amp;1!:1"N!(!!6!"J!'U!:1!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!+!!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"5!'5!&lt;1"Q!&amp;1!;1"N!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!=!!!"3!'5!9Q"P!()!:!!R!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!*!!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"F!'U!=!"F!()!91"U!(5!=A"F!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#=!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!&amp;1!:1"N!(!!:1"S!'%!&gt;!"V!()!:1!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!+Q!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"5!'5!&lt;1"Q!'5!=A"B!(1!&gt;1"S!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!A!!!"3!'5!9Q"P!()!:!!R!$!!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!I!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"$!%-!2!"@!%9!;1"M!(1!:1"S!&amp;]!4A"B!'U!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!L!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"$!%-!2!"@!%9!;1"M!(1!:1"S!&amp;]!4A"B!'U!:1!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,Q!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"$!%-!2!"@!%9!;1"M!(1!:1"S!&amp;]!4A"B!'U!:1!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"4!(1!=A"J!'Y!:Q!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$%!-1!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!$%!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!2A"P!'-!&gt;1"T!&amp;]!4!"P!'-!91"U!'E!&lt;Q"O!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!$1!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!2A"P!'-!&gt;1"T!&amp;]!4!"P!'-!91"U!'E!&lt;Q"O!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!Y!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!2A"P!'-!&gt;1"T!&amp;]!4!"P!'-!91"U!'E!&lt;Q"O!!91!!!%!!!!6!"Z!(!!:1!'%!!!"1!!!%E!&lt;A"U!$-!-A!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$%!-A!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!$!!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!2A"P!'-!&gt;1"T!&amp;]!5A"F!'1!&gt;1"D!'5!=A!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!T!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!%9!&lt;Q"D!(5!=Q"@!&amp;)!:1"E!(5!9Q"F!()!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$=!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q"'!']!9Q"V!(-!8Q"3!'5!:!"V!'-!:1"S!!91!!!%!!!!6!"Z!(!!:1!'%!!!"A!!!&amp;-!&gt;!"S!'E!&lt;A"H!!91!!!)!!!!5A"F!'-!&lt;Q"S!'1!-1!T!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!+1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!31"N!'%!:Q"F!&amp;]!31"N!'%!:Q"F!&amp;]!6!"Z!(!!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!M!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"*!'U!91"H!'5!8Q"*!'U!91"H!'5!8Q"5!(E!=!"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!Q!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%E!&lt;1"B!'=!:1"@!%E!&lt;1"B!'=!:1"@!&amp;1!?1"Q!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!A!!!"3!'5!9Q"P!()!:!!R!$1!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!N!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q".!'%!?!"@!%)!=A"J!'=!;!"U!'Y!:1"T!(-!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!-!!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!31"N!'%!:Q"F!&amp;]!41"B!(A!8Q"#!()!;1"H!'A!&gt;!"O!'5!=Q"T!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!U!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%E!&lt;1"B!'=!:1"@!%U!91"Y!&amp;]!1A"S!'E!:Q"I!(1!&lt;A"F!(-!=Q!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!A!!!"3!'5!9Q"P!()!:!!R!$5!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!F!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!&amp;)!11!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!I!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!&amp;)!11!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,!!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!&amp;)!11!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"%!']!&gt;1"C!'Q!:1!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$%!.A!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!#U!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!5Q"D!']!=!"F!&amp;]!61"T!'5!:!!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!Q!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!&amp;-!9Q"P!(!!:1"@!&amp;5!=Q"F!'1!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$1!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q"4!'-!&lt;Q"Q!'5!8Q"6!(-!:1"E!!91!!!%!!!!6!"Z!(!!:1!'%!!!"A!!!&amp;-!&gt;!"S!'E!&lt;A"H!!91!!!)!!!!5A"F!'-!&lt;Q"S!'1!-1!X!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!(1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"J!'U!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!A!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'E!&lt;1"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!E!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!;1"N!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!A!!!"3!'5!9Q"P!()!:!!R!$A!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!R!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q"5!']!&gt;!"B!'Q!8Q"1!'E!?!"F!'Q!8Q"/!(5!&lt;1"C!'5!=A!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!U!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"*!'U!91"H!'5!8Q"5!']!&gt;!"B!'Q!8Q"1!'E!?!"F!'Q!8Q"/!(5!&lt;1"C!'5!=A!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!/!!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"*!'U!91"H!'5!8Q"5!']!&gt;!"B!'Q!8Q"1!'E!?!"F!'Q!8Q"/!(5!&lt;1"C!'5!=A!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!A!!!"3!'5!9Q"P!()!:!!R!$E!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!I!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q"9!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!L!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"*!'U!91"H!'5!8Q"9!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,Q!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"*!'U!91"H!'5!8Q"9!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!=!!!"3!'5!9Q"P!()!:!!S!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!*A!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q""!'Q!&gt;!!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!J!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!%%!&lt;!"U!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!N!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!11"M!(1!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!A!!!"3!'5!9Q"P!()!:!!S!$!!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!M!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q"9!&amp;]!5!"J!(A!:1"M!&amp;]!6Q"J!'1!&gt;!"I!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#]!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%E!&lt;1"B!'=!:1"@!&amp;A!8Q"1!'E!?!"F!'Q!8Q"8!'E!:!"U!'A!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$-!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!31"N!'%!:Q"F!&amp;]!7!"@!&amp;!!;1"Y!'5!&lt;!"@!&amp;=!;1"E!(1!;!!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!A!!!"3!'5!9Q"P!()!:!!S!$%!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!I!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q":!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!L!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"*!'U!91"H!'5!8Q":!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,Q!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"*!'U!91"H!'5!8Q":!&amp;]!1A"J!'Y!&lt;A"J!'Y!:Q!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!A!!!"3!'5!9Q"P!()!:!!S!$)!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!M!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"*!'U!91"H!'5!8Q":!&amp;]!5!"J!(A!:1"M!&amp;]!6Q"J!'1!&gt;!"I!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#]!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%E!&lt;1"B!'=!:1"@!&amp;E!8Q"1!'E!?!"F!'Q!8Q"8!'E!:!"U!'A!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$-!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!31"N!'%!:Q"F!&amp;]!71"@!&amp;!!;1"Y!'5!&lt;!"@!&amp;=!;1"E!(1!;!!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!T!$)!"B!!!!A!!!"3!'5!9Q"P!()!:!!S!$-!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!K!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"$!%-!2!"@!%5!?!"Q!']!=Q"V!()!:1"@!&amp;1!;1"N!'5!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!,1!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!1Q"$!%1!8Q"&amp;!(A!=!"P!(-!&gt;1"S!'5!8Q"5!'E!&lt;1"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!R!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%-!1Q"%!&amp;]!21"Y!(!!&lt;Q"T!(5!=A"F!&amp;]!6!"J!'U!:1!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"%!']!&gt;1"C!'Q!:1!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$)!.!!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!$9!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!:1"B!()!9Q"I!&amp;]!5A"F!(-!&gt;1"M!(1!=Q"@!%E!&lt;A"E!'5!?!!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!Z!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"4!'5!91"S!'-!;!"@!&amp;)!:1"T!(5!&lt;!"U!(-!8Q"*!'Y!:!"F!(A!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$U!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"3!'5!=Q"V!'Q!&gt;!"T!&amp;]!31"O!'1!:1"Y!!91!!!%!!!!6!"Z!(!!:1!'%!!!"A!!!&amp;5!31"O!(1!-Q!S!!91!!!)!!!!5A"F!'-!&lt;Q"S!'1!-A!V!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!-1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"$!']!&lt;1"Q!'Q!:1"U!'5!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!.!!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"$!']!&lt;1"Q!'Q!:1"U!'5!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$A!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"$!']!&lt;1"Q!'Q!:1"U!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!(!!!!1A"P!']!&lt;!"F!'%!&lt;A!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$)!.A!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!#A!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!&gt;1"C!'U!;1"U!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#M!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!&gt;1"C!'U!;1"U!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!P!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!&gt;1"C!'U!;1"U!!91!!!%!!!!6!"Z!(!!:1!'%!!!"Q!!!%)!&lt;Q"P!'Q!:1"B!'Y!"B!!!!A!!!"3!'5!9Q"P!()!:!!S!$=!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!F!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"%!'%!&gt;!"B!')!91"T!'5!8Q"4!&amp;1!4!!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!I!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"4!&amp;1!4!!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,!!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"4!&amp;1!4!!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"4!(1!=A"J!'Y!:Q!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$)!/!!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!#]!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!:1"B!()!9Q"I!&amp;]!6A"B!'Q!&gt;1"F!(-!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!-A!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"7!'%!&lt;!"V!'5!=Q!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!.A!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"4!'5!91"S!'-!;!"@!&amp;9!91"M!(5!:1"T!!91!!!%!!!!6!"Z!(!!:1!'%!!!"A!!!&amp;-!&gt;!"S!'E!&lt;A"H!!91!!!)!!!!5A"F!'-!&lt;Q"S!'1!-A!Z!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!,Q!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!2!"B!(1!91"C!'%!=Q"F!&amp;]!5Q"F!'%!=A"D!'A!8Q"$!']!&lt;!"V!'U!&lt;A!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!S!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"4!'5!91"S!'-!;!"@!%-!&lt;Q"M!(5!&lt;1"O!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!W!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%1!91"U!'%!9A"B!(-!:1"@!&amp;-!:1"B!()!9Q"I!&amp;]!1Q"P!'Q!&gt;1"N!'Y!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!=!!!"3!'5!9Q"P!()!:!!T!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!*1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q""!(I!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!+!!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q""!(I!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!#Q!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q""!(I!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!A!!!"3!'5!9Q"P!()!:!!T!$!!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!I!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"%!'%!&gt;!"B!')!91"T!'5!8Q"0!(5!&gt;!"Q!(5!&gt;!!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!L!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"0!(5!&gt;!"Q!(5!&gt;!!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,Q!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"%!'%!&gt;!"B!')!91"T!'5!8Q"0!(5!&gt;!"Q!(5!&gt;!!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"4!(1!=A"J!'Y!:Q!'%!!!#!!!!&amp;)!:1"D!']!=A"E!$-!-1!-%!!!"Q!!!!91!!!&amp;!!!!11"5!(E!=!"F!!91!!!+!!!!5A"F!'%!:!!P!&amp;=!=A"J!(1!:1!'%!!!"1!!!%)!6!"Z!(!!:1!'%!!!$Q!!!&amp;!!=A"P!'I!:1"D!(1!)!"#!'E!&lt;A"E!'E!&lt;A"H!!91!!!*!!!!1Q"V!(-!&gt;!"P!'U!;1"[!'5!!A!!!!!'%!!!"!!!!%Y!91"N!'5!"B!!!#5!!!"Q!'A!:!"P!'U!:1!N!(!!9Q!R!$I!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1![!%E!&lt;1"B!'=!:1"@!%%!&gt;1"U!'A!&lt;Q"S!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#A!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%E!&lt;1"B!'=!:1"@!%%!&gt;1"U!'A!&lt;Q"S!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!M!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%E!&lt;1"B!'=!:1"@!%%!&gt;1"U!'A!&lt;Q"S!!91!!!%!!!!6!"Z!(!!:1!'%!!!"A!!!&amp;-!&gt;!"S!'E!&lt;A"H!!91!!!)!!!!5A"F!'-!&lt;Q"S!'1!-Q!S!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!(!!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!5Q"5!%Q!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!(Q!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!5Q"5!%Q!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!#-!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!5Q"5!%Q!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!A!!!"3!'5!9Q"P!()!:!!T!$-!$"!!!!=!!!!'%!!!"1!!!%%!6!"Z!(!!:1!'%!!!#A!!!&amp;)!:1"B!'1!,Q"8!()!;1"U!'5!"B!!!!5!!!"#!&amp;1!?1"Q!'5!"B!!!!]!!!"1!()!&lt;Q"K!'5!9Q"U!#!!1A"J!'Y!:!"J!'Y!:Q!'%!!!#1!!!%-!&gt;1"T!(1!&lt;Q"N!'E!?A"F!!)!!!!!"B!!!!1!!!"/!'%!&lt;1"F!!91!!!M!!!!=!"I!'1!&lt;Q"N!'5!,1"Q!'-!-1![!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!/A"%!'%!&gt;!"B!')!91"T!'5!8Q"$!']!&lt;A"E!'E!&gt;!"J!']!&lt;A"T!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#]!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%1!91"U!'%!9A"B!(-!:1"@!%-!&lt;Q"O!'1!;1"U!'E!&lt;Q"O!(-!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!$-!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!2!"B!(1!91"C!'%!=Q"F!&amp;]!1Q"P!'Y!:!"J!(1!;1"P!'Y!=Q!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!5!!!"*!'Y!&gt;!!R!$9!"B!!!!=!!!"3!'5!9Q"P!()!:!!U!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!,1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!1Q"$!%1!8Q"$!'%!&lt;1"F!()!91"@!%)!;1"U!&amp;]!2!"F!(!!&gt;!"I!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!$!!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%-!1Q"%!&amp;]!1Q"B!'U!:1"S!'%!8Q"#!'E!&gt;!"@!%1!:1"Q!(1!;!!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!.!!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"$!%-!2!"@!%-!91"N!'5!=A"B!&amp;]!1A"J!(1!8Q"%!'5!=!"U!'A!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!=!!!"3!'5!9Q"P!()!:!!V!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!+!!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!1Q"$!%1!8Q"$!'%!&lt;1"F!()!91"@!%Y!91"N!'5!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!+Q!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!1Q"$!%1!8Q"$!'%!&lt;1"F!()!91"@!%Y!91"N!'5!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!#]!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!1Q"$!%1!8Q"$!'%!&lt;1"F!()!91"@!%Y!91"N!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!=!!!"3!'5!9Q"P!()!:!!W!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!+!!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!1Q"$!%1!8Q"$!']!&lt;Q"M!'5!=A"@!&amp;1!:1"N!(!!"B!!!!9!!!"/!'5!&gt;!"6!&amp;)!4!!'%!!!+Q!!!&amp;Q!8!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!&amp;Q!1Q"$!%1!8Q"$!']!&lt;Q"M!'5!=A"@!&amp;1!:1"N!(!!"B!!!!A!!!"1!()!&lt;Q"K!&amp;!!91"U!'A!"B!!!#]!!!".!(E!)!"$!']!&lt;1"Q!(5!&gt;!"F!()!8!"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!#Y!&lt;!"W!'Q!;1"C!&amp;Q!1Q"$!%1!8Q"$!']!&lt;Q"M!'5!=A"@!&amp;1!:1"N!(!!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!=!!!"3!'5!9Q"P!()!:!!X!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!(1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!2!"B!(1!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!A!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"%!'%!&gt;!"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!E!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!%1!91"U!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!"B!!!!=!!!"3!'5!9Q"P!()!:!!Y!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!*A!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"F!'Q!:1"T!'-!&lt;Q"Q!'5!8Q"%!'5!9Q!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!J!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'5!&lt;!"F!(-!9Q"P!(!!:1"@!%1!:1"D!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!N!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!:1"M!'5!=Q"D!']!=!"F!&amp;]!2!"F!'-!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!=!!!"3!'5!9Q"P!()!:!!Z!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!*Q!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!31"N!'%!:Q"F!&amp;]!2A"J!'Q!:1"Q!'%!&gt;!"I!!91!!!'!!!!4A"F!(1!61"3!%Q!"B!!!#I!!!"=!&amp;Q!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1"=!%E!&lt;1"B!'=!:1"@!%9!;1"M!'5!=!"B!(1!;!!'%!!!#!!!!&amp;!!=A"P!'I!5!"B!(1!;!!'%!!!,A!!!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!,A"M!(9!&lt;!"J!')!8!"*!'U!91"H!'5!8Q"'!'E!&lt;!"F!(!!91"U!'A!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!5Q"U!()!;1"O!'=!</Property>
		<Property Name="className" Type="Str">EPICS Server</Property>
	</Item>
	<Item Name="Image_Author" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Filepath" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Image_Type" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Max_Brightness" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Total_Pixel_Number" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_X_Binning" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_X_Pixel_Width" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Y_Binning" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Image_Y_Pixel_Width" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="STL" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Alt" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Az" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Dec" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Focus_Location" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!D(Q!!!"=!A!!!!!!"!!N!!Q!&amp;37ZU-T)!!1!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Focus_Reducer" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_RA" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Telescope_Scope_Used" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Temperature" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="TempTime" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Time" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!I*!!!!"=!A!!!!!!"!""!-0````]'5X2S;7ZH!!!"!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
