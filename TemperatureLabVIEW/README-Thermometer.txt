Temperature Measurement using NI myDAQ

--------------------------

Calibrated for Texas Instruments LM35DZ temperature sensor

--------------------------

Software required:
	LabVIEW 2017 (32-bit)
	NI DAQmx (17.1)
	LabVIEW Real-Time English (2017)