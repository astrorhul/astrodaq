Database Front Panel for python code

-----------------------

Run 3 commands on iPython
>> cd \Users\domeuser\Desktop\Database
>> import SQLite_Database
>> db = SQLite_Database.Database()

(sometimes need to execute last command twice due to 'identical process 
variable' error)

Open LabVIEW VI's: 'DatabaseSearch' & 'DatabaseSaving'

Run both continuously

-----------------------

Database FrontPanel in LabVIEW communicates with python 'SQLite_Database.py'
via shared variables on EPICS server.

LabVIEW hosts server for variables
	-deploys each time FrontPanel is run
	-must be 'Run Continuously' to read/write in real time

-----------------------

Python monitor LabVIEW via instructions from callbacks.

Callbacks exist on following controlable variables:
for image saving:
	-'Save Image to Database?' button
for db searching:
	-'Search:' string control
		(searches with one condition)
	-'Submit condition' button 
		(searches with multiple conditions)
	-'Search Result Index' number
		(displays different elements from search results array)

After typing info in, need to press tick button on LabVIEW toolbar

-----------------------

Useful Links:
	-http://sebastianraschka.com/Articles/2014_sqlite_in_python_tutorial.html
	-http://cars9.uchicago.edu/software/python/pyepics3/pv.html

-----------------------

Potential improvements:
-Multiple condition only works for 'AND' statement
-unfinished automatic search
	-searches via RA & Dec and returns array of filenames which overlap
		at current target
	-needs scope & camera info to know RA & Dec range of image
-image display in place but unconnected
-take image button in LabVIEW
-incorporate python control into LabVIEW
-add quit button to LabVIEW - function written in python