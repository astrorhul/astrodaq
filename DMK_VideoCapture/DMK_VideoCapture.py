import numpy
import cv2
from pyicic.IC_ImagingControl import *
import numpy as np

class Camera:
    def __init__(self):
        '''Opens libraries and camera:
           starts live connection to camera'''
        self.openLib()
        self.openCam()
        self.cam.start_live()
        self.cam.enable_continuous_mode(True)

    def openLib(self):
        '''Opens library '''
        self.ic = IC_ImagingControl()
        self.ic.init_library()

    def openCam(self):
        ''' Opens camera which is connected.
        If multiple cameras connected, may need to change 0 to other number in self.cam_name[0]'''
        self.cam_name = self.ic.get_unique_device_names()
        self.cam = self.ic.get_device(self.cam_name[0])
        self.cam.open()

    def setParams(self,exposure = 500,gain = 180):
        ''' sets parameters of camera:
        exposure: exposure of camera in seconds 
        gains: gains of camera'''
        self.cam.exposure.auto = False
        self.cam.gain.austo = False
        self.cam.exposure.value = exposure
        self.cam.gain.value = gain
        print self.cam.exposure.value
        print self.cam.gain.value

    def image(self,image_path = "none"):
        '''Takes image and saves to file path with exposure and gain as set with setParams() 
        image_path: The location to save the image '''
        if image_path == "none":
            print "Image path not set"
            image_path = 'C:/Users/domeuser/Desktop/3_Data/Test/Image1.bmp'
            print "Image path: ", image_path
        else:
            pass
        self.cam.snap_image()
        self.cam.wait_til_frame_ready(100)
        self.cam.save_image(image_path)

    def getImage(self):
        ''' gets image from camera and returns a numpy array of image.
        Parameters set by setParams() function'''
        self.cam.snap_image()
        data = self.cam.get_image_data()
        img = np.ndarray(buffer = data[0],dtype = np.uint8,shape = (data[2],data[1],data[3]))
        return img
        
    def quit(self):
        ''' stops live camera and closes library and camera '''
        self.cam.stop_live()
        self.cam.close()
        self.ic.close_library()
