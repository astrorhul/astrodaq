﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Item Name="EPICS Server1" Type="IO Server">
		<Property Name="atrb" Type="Str">X!-!!!Q1!!!#!!!!"B!!!!=!!!"3!'5!9Q"P!()!:!!Q!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!(1!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6!"J!'U!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!A!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"5!'E!&lt;1"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!E!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!;1"N!'5!"B!!!!1!!!"5!(E!=!"F!!91!!!'!!!!2!"P!(5!9A"M!'5!"B!!!!=!!!"3!'5!9Q"P!()!:!!R!!Q1!!!(!!!!"B!!!!5!!!""!&amp;1!?1"Q!'5!"B!!!!I!!!"3!'5!91"E!#]!6Q"S!'E!&gt;!"F!!91!!!&amp;!!!!1A"5!(E!=!"F!!91!!!0!!!!5!"S!']!;A"F!'-!&gt;!!A!%)!;1"O!'1!;1"O!'=!"B!!!!E!!!"$!(5!=Q"U!']!&lt;1"J!(I!:1!#!!!!!!91!!!%!!!!4A"B!'U!:1!'%!!!)!!!!(!!;!"E!']!&lt;1"F!#U!=!"D!$%!/A"4!'5!=A"W!'5!=A"-!'E!9A"S!'%!=A"Z!$I!6A"P!'Q!&gt;!"B!'=!:1!'%!!!"A!!!%Y!:1"U!&amp;5!5A"-!!91!!!D!!!!8!"=!%U!?1!A!%-!&lt;Q"N!(!!&gt;1"U!'5!=A"=!&amp;-!:1"S!(9!:1"S!%Q!;1"C!()!91"S!(E!8!"7!']!&lt;!"U!'%!:Q"F!!91!!!)!!!!5!"S!']!;A"1!'%!&gt;!"I!!91!!!L!!!!41"Z!#!!1Q"P!'U!=!"V!(1!:1"S!&amp;Q!5Q"F!()!&gt;A"F!()!4!"J!')!=A"B!()!?1!O!'Q!&gt;A"M!'E!9A"=!&amp;1!:1"N!(!!:1"S!'%!&gt;!"V!()!:1!'%!!!"!!!!&amp;1!?1"Q!'5!"B!!!!9!!!"%!']!&gt;1"C!'Q!:1!</Property>
		<Property Name="className" Type="Str">EPICS Server</Property>
	</Item>
	<Item Name="Temperature" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="Time" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!J*1!!!"=!A!!!!!!"!!V!#A!'2'^V9GRF!!!"!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
</Library>
