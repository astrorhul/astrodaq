import win32com.client	
import time
import os
import pyfits

SaturationValues = {'SXV-M7C':34}   #Dictionary of the maximum values for each camera to check for saturation in images

class CameraClass:
	def __init__(self):
		"Initialises camera instance and checks connection is successful. If Error is recieved"
		self.Camera = win32com.client.Dispatch("MaxIm.CCDCamera")
		
		try:
			self.Camera.LinkEnabled = True
			print "Connection Established"
		except:
			print "ERROR: Connection Failed"
			
			
	
	
	def Quit(self):
		self.Camera.LinkEnabled = False
		print "Connection Terminated"
		
	
	def TakeImage(self, ExposureTime, CameraModel):
		
                try: 
                    ExposureTime > 0
                    
		except:
                    print "Invalid Exposure time entered"
                    return
		print "Taking Image..."
		
		
		
		self.Camera.Expose(ExposureTime, 1)
		
		while not self.Camera.ImageReady:
			time.sleep(1)
		print "Complete."
		try :
                    CameraModel in SaturationValues.values()
                    CameraSaturationVal = SaturationValues[CameraModel]
                
                    if GetImageData("TestImage.fit").max() == CameraSaturationVal:
                        print "WARNING: Saturation Detected"
                        
                except:
                    print "Camera Model entered is unrecognised."
                
                print "Saving..."
		
		self.Camera.SaveImage(r"C:\Users\domeadmin\Desktop\Development\TestImage.fit")
		print "Saved Successfully."
                
                
                
                
                
			
        
                
                            
                            
def GetImageData(filename):
    f = pyfits.open(filename)
    ImageData = f[0].data
    return ImageData
    
                