README file for Telescope_Control.py

Uses the Ascom POTH interface to control the telescope and focuser via python
-----------------------------------------------------------------
Usage:

--ensure the telescope is connected to the pc
--press 0 on handset to begin GPS Fix
--once complete, navigate to top level menu via MENU button and then select via ENTER
Objects -> Star -> LONG PRESS(<star telescope is aimed to>) -> PRESS ENTER TO SYNC

Then import file and initialise via:
Tele = Telescope_Control.Telescope()

wait for the connection established message

then methods can be viewed via the TAB key.

e.g.

Tele.<TAB>

SetScope  GetFocuserTemp  MoveFocus ...etc



----------------------------------------------------------------
