'''
Camera Control Class, uses MaxImDL
'''

import win32com.client
import time
import datetime
import os
import pyfits
from epics import PV

SaturationValues = {'SX Universal':65535,'placeholderfor_MeadeLPI_':255,'placeholderfor_DMK21AF04':255,'placeholderfor_Logitech_': 255, 'placeholderfor_SBIG16bitCameras_':65535}   #Dictionary of the maximum values for each camera to check for saturation in images
CameraBitDepths = {'SX Universal': 16, }

class Camera:

	DateTime = {}
	def __init__(self):
		"Initialises camera instance and checks connection is successful. If Error is recieved"
		self.Camera = win32com.client.Dispatch("MaxIm.CCDCamera")


		self._FrameType = "Light"
		self._ExposureTime = 0
		try:
			self.Camera.LinkEnabled = True

			print "Connection Established"
		except:
			print "ERROR: Connection Failed"




	def Quit(self):
		"Disconnects Camera and disables cooler and fans"
		try:
			self.Camera.CoolerOn = False
			self.Camera.FanEnabled = False
			self.Camera.LinkEnabled = False
			print "Connection Terminated"
		except:
			print "Camera could not be disconnected."





	def SetSubFrame(self, StartX, StartY, NumX, NumY):
		"""
		Sets a rectangular Subframe by starting at A[StartX,StartY]
		then going to B[StartX + NumX,StartY + NumY]
		A______________
		|       |      |
		|       |      |
		|	    |      |
		|_______|B     |
		|              |
		|______________|
		"""
		self.Camera.NumX = NumX
		self.Camera.NumY = NumY
		self.Camera.StartX = StartX
		self.Camera.StartY = StartY
		print "Subframe: A[{},{}], B[{},{}]".format(StartX,StartX+NumX,StartY,StartY+NumY)


	def TakeImage(self, ExposureTime, LightOrDark):
		'''
		ExposureTime is the Length of time the CCD will be exposed to Light
		LightOrDark:
		-Light Frame: 1
		-Dark Frame: 0
		'''


		try:
			float(ExposureTime)
		except:
			print "ERROR: Invalid Exposure time entered"
			return
		if ExposureTime < 0:
		    print "ERROR: Invalid Exposure time entered"
		    return
		self._ExposureTime = ExposureTime
		print "Taking Image..."
		self.Camera.Expose(ExposureTime, LightOrDark)

		while not self.Camera.ImageReady: time.sleep(1)

		print "Complete."
		try:
			CameraModel = str(self.Camera.CameraName)
			CameraModel in SaturationValues.values()
			CameraSaturationVal = SaturationValues.get(CameraModel)

			if self.Camera.ImageArray.max() >= float(CameraSaturationVal) * 0.9:   #Value set to 90% of max, warning dead pixels etc will give false warning
			    print "WARNING: Saturation Detected"


			    cont = raw_input("Save Image? y/n:")

			    if cont != 'y':
			        print "Image not Saved."
			        return

		except:
			print "Camera Model entered is unrecognised."
			print "Saturation could not be detected."

			cont = raw_input("Save Image? y/n:")
			if cont != 'y':
			    print "Image not Saved."
			    return

			print "Saving..."
			timestring = time.strftime("%Y%m%d-%H%M%S")
			timestring = timestring + "_"
			#DestinationPath = u"C:\Users\domeuser\Desktop\3_Data\DB_Image_Storage\"
			self.Camera.SaveImage( "test.fits" )
			print "Saved Successfully."
	def SetCooler(self,PowerBool):
		'''
		Turns the cooler on the CCD on or off
		Parameter is Boolean (True/False)
		'''
		if PowerBool:
		        self.Camera.CoolerOn = True
		        print "Cooler Active"
		elif not PowerBool:
		        self.Camera.CoolerOn = False
		        print "Cooler Inactive"
		else:
		        print  "ERROR: Entry Invalid"

	def SetFan(self,FanBool):
		try:
			if PowerBool:
				self.FanEnabled = True
				print "Fan enabled"
			elif PowerBool:
				self.FanEnabled = False
				print "Fan disabled"
			else:
				print "Invalid Entry"
		except:
			print "camera model is not compatible with fan control"
	def GetCCDTemp(self):
		return self.Camera.Temperature
	def SetCCDTemp(self, Temp):
		try:
			float(Temp)
			if((Temp < 100.0) and (Temp > -273.0)):
				self.Camera.TemperatureSetpoint(Temp)
			else:
				print "Temperature not in Acceptable range (-273C to 100C)"
		except:
			print "ERROR: Invalid Temperature Entered."

	def SetBinning(self, X, Y):
	    if  (not X.isdigit()) or (not Y.isdigit()) or  (X > self.Camera.MaxBinX) or  (Y > self.Camera.MaxBinY) or (X < 1) or (Y < 1) :
	        print "Invalid X, Y values"
	        return


	    try:
	        XYBinning = True
	    except:
	        print "ERROR: Couldn't Enable XYBinning"

	    try:
	    	self.Camera.BinX = X
	        self.Camera.BinY = Y
	    except:
	        print "ERROR: Couldn't set Bin values"


	def TakeMultiple(self, Number ,ExposureTime, LightOrDark):
		'''
		Method to allow sequences of images to be taken, not yet made.
		'''
		pass

	def LoadLastImage(self):
	    '''
	    Returns array(long) of pixel values from last taken image
	    '''
	    IArray = self.Camera.ImageArray
	    return IArray

	def SetLastFrameType(self, Type):
		'''
		0: Light
		1: Dark
		2: Flat
		3: Bias
		'''

		if Type == 0:
			self._FrameType = "Light"
		elif Type == 1:
			self._FrameType = "Dark"
		elif Type == 2:
			self._FrameType = "Flat"
		elif Type == 3:
			self._FrameType = "Bias"
		else:
			print "ERROR: No Frame Type Set. Enter 0,1,2,3 for Light,Dark,Flat,Bias"
			self._FrameType = None


	def ImageDataToDatabase(self, Telescope):
		'''Collects all relevant data to send to database'''


		Filepath = PV('phdome-pc1:ServerLibrary:Image_Filepath', auto_monitor = True)
		CamName = PV('phdome-pc1:ServerLibrary:CCD_Camera_Name', auto_monitor = True)
		FilterName = PV('phdome-pc1:ServerLibrary:CCD_Filter_Name', auto_monitor = True)
		XBinning = PV('phdome-pc1:ServerLibrary:Image_X_Binning', auto_monitor = True)
		YBinning = PV('phdome-pc1:ServerLibrary:Image_Y_Binning', auto_monitor = True)
		CenterRA = PV('phdome-pc1:ServerLibrary:Telescope_RA', auto_monitor = True)
		CenterDec = PV('phdome-pc1:ServerLibrary:Telescope_Dec', auto_monitor = True)
		Date = PV('phdome-pc1:ServerLibrary:Date', auto_monitor = True)
		Time = PV('phdome-pc1:ServerLibrary:Time', auto_monitor = True)
		CenterAlt = PV('phdome-pc1:ServerLibrary:Telescope_Alt', auto_monitor = True)
		CenterAz = PV('phdome-pc1:ServerLibrary:Telescope_Az', auto_monitor = True)
		BitDepth = PV('phdome-pc1:ServerLibrary:CCD_Camera_Bit_Depth', auto_monitor = True)
		CoolerTemp = PV('phdome-pc1:ServerLibrary:CCD_Cooler_Temp', auto_monitor = True)
		FocusPosition = PV('phdome-pc1:ServerLibrary:Telescope_Focus_Location', auto_monitor = True)
		FocusReducer = PV('phdome-pc1:ServerLibrary:Telescope_Focus_Reducer', auto_monitor = True)
		FrameType = PV('phdome-pc1:ServerLibrary:Image_Image_Type', auto_monitor = True)
		MaxBrightness = PV('phdome-pc1:ServerLibrary:Image_Max_Brightness', auto_monitor = True)
		ScopeUsed = PV('phdome-pc1:ServerLibrary:Telescope_Scope_Used', auto_monitor = True)
		TotalPixels = PV('phdome-pc1:ServerLibrary:Image_Total_Pixel_Number', auto_monitor = True)
		XPWidth = PV('phdome-pc1:ServerLibrary:Image_X_Pixel_Width', auto_monitor = True)
		YPWidth = PV('phdome-pc1:ServerLibrary:Image_Y_Pixel_Width', auto_monitor = True)
		ExpTime = PV('phdome-pc1:ServerLibrary:CCD_Exposure_Time', auto_monitor= True)

		NOW = datetime.datetime.now()

		Filepath.put(self.Camera.Document.FilePath)
		CamName.put(self.Camera.CameraName)
		BitDepth.put(CameraBitDepths.get(CamName, 0))
		FilterName.put(self.Camera.FilterWheelName)
		XBinning.put(self.Camera.BinX)
		YBinning.put(self.Camera.BinY)
		#CenterRA.put(Telescope.telescope.GetRADec[0])
		#CenterDec.put(Telescope.telescope.GetRADec[1])
		#CenterAlt.put(Telescope.RADecToAltAz(CentreRA.get(), CenterDec.get())[0])
		#CenterAz.put(Telescope.RADecToAltAz(CentreRA.get(), CenterDec.get())[1])
		Date.put(str(NOW.year) + ":" + str(NOW.month) + ":" + str(NOW.day))
		Time.put(str(NOW.hour) + ":" + str(NOW.minute) + ":" + str(NOW.second))
		#ScopeUsed.put(Telescope._ScopeName)
		MaxBrightness.put(self.Camera.MaxPixel)
		XPWidth.put(self.Camera.NumX)
		YPWidth.put(self.Camera.NumY)
		TotalPixels.put(XPWidth.get() * YPWidth.get())
		FrameType.put(self._FrameType)
		CoolerTemp.put(self.Camera.Temperature)
		#Ambient Temperature - LABVIEW
		#FocusPosition.put(Telescope.focuser.Position)
		#FocalReducer.put(Telescope._FocalReducer)
		ExpTime.put(self._ExposureTime)





'''

def GetImageData(filename):
    f = pyfits.open(filename)
    ImageData = f[0].data
    return ImageData
'''
