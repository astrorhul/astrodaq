import win32com.client
import time
import datetime

from astropy.coordinates import EarthLocation
from astropy.coordinates import SkyCoord
from astropy.coordinates import AltAz
from astropy.time import Time

"""Telescope Control Class with method for movement via ASCOM POTH """

DomeLocation = EarthLocation(lat = 51.426926, lon = -0.563191)


class Telescope:
    def __init__(self):
        '''
        Initialises object and creates connection, takes no args
        '''
        self.telescope = win32com.client.Dispatch("POTH.Telescope")
        self.focuser = win32com.client.Dispatch("POTH.Focuser")

        self._FocalReducer = None
        self._ScopeName = None

        try:
            self.telescope.connected = True
            print "Telescope Connected"
        except:
            print "ERROR: Telescope Connection Failed"

        try:
            self.focuser.link = True
            print "Focuser Connected"
        except:
            print "ERROR: Focuser Connection Failed"

        if self.telescope.connected :
            print "Current RA (hours): ", self.telescope.rightascension
            print "Current Dec (degrees): ", self.telescope.declination

        try:
            self.SetScope(raw_input("Enter the Telescope Name:"))
        except:
            print "Could not set Set Scope Name, name invalid. Set to UNKNOWN."
            self._ScopeName = "UNKNOWN"
    def Quit(self):
        '''
        Attempts to disconnect from telescope and focuser
        '''
        try:
            self.telescope.Tracking = False
            self.focuser.link = False
            self.telescope.connected = False
        except:
            print "ERROR: Could not disconnect"
    def GetRADec(self):
        '''
        returns current RA and Dec in decimal form
        '''
        print "Current RA (hours): ", self.telescope.rightascension
        print "Current Dec (degrees): ", self.telescope.declination

        return [self.telescope.rightascension,self.telescope.declination]

    def GetAltAz(self):
        return [self.telescope.altitude, self.telescope.azimuth]

    def Tracking(self, Bool):
        '''
        Allows user to change Tracking state
        '''

        if not(type(True) == type(Bool)):
            print "Incorrect type entered. must be True or False"
        try:
            self.telescope.Tracking = Bool
        except:
            if Bool: print "ERROR: Tracking could not enable"
            if not(Bool): print "ERROR: Tracking could not disable"


    def Slew(self, RA, Dec):
        '''WARNING: SAFETY NOT ADDED, BE READY TO TURN OFF IF COORDINATES ARE DANGEROUS'''
        Az = 0
        Alt = 0

        if ((Az < 90) or (Az > 270)):
            print "Unsafe Coordinates entered. Slew Aborted"
            return
        elif ((Alt < 0) or (Alt > 60)):
            print "Unsafe Coordinates entered. Slew Aborted"
            return


        self.telescope.slewtocoordinates(RA,Dec)
        print "Slew Complete"

    def GetFocuserParams(self):
        print "Max step size: ", self.focuser.maxincrement
        print "Max position: ", self.focuser.maxstep

    def MoveFocus(self, distance):
        self.focuser.move(distance)
        print "New Position: ", self.focuser.position
    def GetFocuserTemp(self):
        return self.focuser.Temperature

    def SetScope(self, Name):
        try:
            self._ScopeName = Name
            print "ScopeName set to: " + self._ScopeName
        except:
            print "Scope Name could not be set, please enter as string"
            self._ScopeName = None

    def SetFocalReducerName(self, Name):
        try:
            self._FocalReducer = Name
            print "Focal Reducer set to: " + self._FocalReducer
        except:
            print "Focal Reducer could not be set, please enter as string"
            self._FocalReducer = None

    def RADecToAltAz(RA,Dec):
        TimeNow = Time(datetime.datetime.utcnow())
        AA = AltAz(location = DomeLocation, obstime = TimeNow)
        Coords = SkyCoord(RA,Dec)
        Coords.transform_to(AA)
        ALTAZ = Coords.to_string('decimal')

        return ALTAZ
