This is the ReadMe for Camera_Control.py
-------------------------------------------
Usage:
The program uses the MaximDL scripting interface, specifically the Application,
CCDCamera, and Document objects and controls these using the Python language.

To initialise a camera object first ensure the camera is connected to the PC and powered on.
then instantiate it:

Cam1 = Camera_Control.Camera()

if a connection is established this creates the CCDCamera Object and its methods and member variables
can be accessed via the Tab key. The main function is to Take images by the
TakeImage method.

Currently no filepath is specified but a simple test.fits can be used for testing purposes.
The object stores its previous image so attributes can be accessed and read out.




-------------------------------------------
Possible Additions:

--Distinguishing between 2 different cameras is not possible and it is seen that the program connects to the first available camera.
--
