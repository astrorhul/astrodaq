'''
RA DEC Classes
'''

#import datetime
#from astropy.coordinates import EarthLocation
#from astropy.coordinates import SkyCoord
#from astropy.coordinates import AltAz
#from astropy.time import Time

class RADec(object):
    def __init__(self,*args):
        self.RA = 0
        self.Dec = 0
        if (len(args) == 2): #RADec(RA,Dec)
            self.RA = args[0]
            self.Dec = args[1]
        if (len(args) == 6):   #RADec(RH,RM,RS,DD,DM,DS)
            try:
                self.RA = RA(args[0:3])
                self.Dec = RA(args[3:6])
            except:
                print "ERROR: Could not construct RADec"






class RA(object):
    def __init__(self,*args):
        self.hour = 0.0
        self.minute = 0.0
        self.second = 0.0

        if len(args) == 1 and (type(args[0]) == str):
            StringInput = args[0]
            StringInput = StringInput.lower()
            tempstring = ""


            HMS = {0:'h', 1:'m', 2:'s'}
            try:
                HourEntry = StringInput.split('h')[0]
                if HourEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
                StringInput = StringInput.split('h')[1]

                MinuteEntry = StringInput.split('m')[0]
                if MinuteEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
                StringInput = StringInput.split('m')[1]

                SecondEntry = StringInput.split('s')[0]
                if SecondEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
            except:
                print "ERROR: invalid string entry"

            try:
                HourEntry = float(HourEntry)
                MinuteEntry = float(MinuteEntry)
                SecondEntry = float(SecondEntry)
            except:
                raise ValueError('Could not convert to string')

            if HourEntry < 0 or MinuteEntry < 0 or SecondEntry < 0:
                raise ValueError('Negative value entered. Not allowed in RA system.')
            if (((HourEntry < 24) and (MinuteEntry < 60) and(SecondEntry < 60)) or ((HourEntry == 24) and (MinuteEntry == 0) and (SecondEntry == 0))):
                self.hour = HourEntry
                self.minute = MinuteEntry
                self.second = SecondEntry
            else:
                raise ValueError('Values are too large: hour must be <= 24 and minute/second < 60')
        elif len(args) == 3:
            if ((args[0] < 24)and (args[1] < 60) and (args[2] < 60)) or ((args[0] == 24) and (args[1] == 0 ) and (args[2] == 0)):
                self.hour= float(args[0])
                self.minute = float(args[1])
                self.second = float(args[2])
            else:
                raise ValueError('Values are too large: hour must be <= 24 and minute/second < 60')
        else:
            raise ValueError('Entry was not of allowed types')

    def __repr__(self):
        return str(self.hour) + ' hours ' + '\n' + str(self.minute) + ' minutes ' + '\n' + str(self.second) + ' seconds '



    def __add__(self,other):
        Output = RA(0,0,0)
        NewSec = self.second + other.second
        if NewSec > 60:
            Output.second = NewSec % 60
            self.minute += 1
        else:
            Output.second = NewSec
        NewMin = self.minute + other.minute

        if NewMin > 60:
            Output.minute = NewMin % 60
            self.hour += 1
        else:
            Output.minute = NewMin
        NewHour = self.hour + other.hour

        if NewHour > 24:
            Output.hour = NewHour % 24
        else:
            Output.hour = NewHour
        return Output

    def __sub__(self,other):
        Output = RA(0,0,0)

        if other.second <= self.second:
            Output.second = self.second - other.second
        else:
            Output.minute -= 1
            Output.second += 60
            Output.second += self.second - other.second
        if other.minute <= self.minute:
            Output.minute += self.minute - other.minute
        else:
            Output.hour -= 1
            Output.minute += 60
            Output.minute += self.second - other.second

        if other.hour <= self.hour:
            Output.hour = Output.hour + self.hour - other.hour
        if (Output < RA(0,0,0)):
            Output = Output + RA(24,0,0)

        return Output

    def __lt__(self,other):
        if self.hour < other.hour:
            return True
        elif (self.hour == other.hour) and (self.minute < other.minute):
            return True
        elif (self.minute == other.minute) and (self.second < other.second):
            return True
        else:
            return False
    def __gt__(self,other):
        if self.hour > other.hour:
            return True
        elif (self.hour == other.hour) and (self.minute > other.minute):
            return True
        elif (self.minute == other.minute) and (self.second > other.second):
            return True
        else:
            return False

class Dec(object):
    def __init__(self,*args):
        self.degree = 0.0
        self.arcmin = 0.0
        self.arcsec = 0.0

        if len(args) == 1 and (type(args[0]) == str):
            StringInput = args[0]
            StringInput = StringInput.lower()
            tempstring = ""


            DMS = {0:'d', 1:'m', 2:'s'}
            try:
                DegreeEntry = StringInput.split('d')[0]
                if DegreeEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
                StringInput = StringInput.split('d')[1]

                MinuteEntry = StringInput.split('m')[0]
                if MinuteEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
                StringInput = StringInput.split('m')[1]

                SecondEntry = StringInput.split('s')[0]
                if SecondEntry == StringInput:
                    raise ValueError('ERROR: invalid string entry')
            except:
                print "ERROR: invalid string entry"

            try:
                DegreeEntry = float(DegreeEntry)
                MinuteEntry = float(MinuteEntry)
                SecondEntry = float(SecondEntry)
            except:
                raise ValueError('ERROR: invalid string entry, could not convert to float')
            if (((abs(DegreeEntry) < 90) and (MinuteEntry < 60) and(SecondEntry < 60)) or ((abs(DegreeEntry == 90)) and (MinuteEntry == 0) and (SecondEntry == 0))):
                self.degree = DegreeEntry
                self.arcmin = MinuteEntry
                self.arcsec = SecondEntry
            else:
                raise ValueError('genericerrorValues are too large: degree must be <= 24 and minute/second < 60')
        elif len(args) == 3:
            if ((abs(args[0]) < 90)and (args[1] < 60) and (args[2] < 60)) or ((abs(args[0] == 90)) and (args[1] == 0 ) and (args[2] == 0)):
                self.degree= float(args[0])
                self.arcmin = float(args[1])
                self.arcsec = float(args[2])
            else:
                raise ValueError('error')
        else:
            raise ValueError('Entry was not of allowed types')

    def __add__(self,other):
        Result = Dec(0,0,0)
        Result.arcsec = self.arcsec + other.arcsec
        Result.arcmin = self.arcmin + other.arcmin
        Result.degree = self.degree + other.degree
        if Result.arcsec >= 60:
            Result.arcsec = Result.arcsec % 60
            Result.arcmin += 1
        if Result.arcmin >= 60:
            Result.arcmin = Result.arcmin % 60
            Result.degree += 1

    def __sub__(self,other):
        Result = Dec(0,0,0)
