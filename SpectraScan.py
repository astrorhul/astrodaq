import numpy as np
from MaximDL import Maxim_camera_Client as client
from Spectrometer.PyAPT import APTMotor
import time
import datetime

cam = client.connect()
motor = APTMotor()

def SetCamParams():
    cam.setCCDTemp(5)
    cam.setCoolerStatus('on')

def ZeroMotor():
    try:
        motor.go_home()
    except ValueError:
        pass

def SetSubFrame():
    cam.setSubFrame(382,100,16,360)

def Scan():
    print "Params set"
    start = datetime.datetime.now()
    for i in np.linspace(5.9,6.5,200):
        motor.mAbs(i)
        pos = motor.getPos()
        print pos
        time.sleep(1)
        cam.exposeCamera(0.1,'light', 'Z:/20160802/ScanTest/Intervals_5.9_6.5_200/{}mm.fit'.format(round(pos,3)))
    end = datetime.datetime.now()
    print "Scan complete"
    print "Time taken: ", end-start

