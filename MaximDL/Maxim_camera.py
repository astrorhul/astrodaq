import win32com.client
import time
import os

class Camera:
    def __init__(self):
        ''' Opens and connects MaxImDL using win32com.client.
        Then connects camera to MaxImDL'''
        self.camera = win32com.client.Dispatch("MaxIm.CCDCamera")
        # for connecting telesocpe to maximdl
        #self.maxim = win32com.client.Dispatch("MaxIm.Application")
        try:
            self.camera.LinkEnabled = True
            print "Camera connected"
        except:
            print "ERROR: Cannot connect to camera"


    def setCCDTemp(self,Temp):
        '''Sets the temperature of the main camera:
           Temp (float): Temperature in degrees Celsius
           '''
        self.CCDTemp = Temp
        self.camera.TemperatureSetpoint = self.CCDTemp
        print "Camera temp setpoint: {} C".format(self.CCDTemp)
    
    def coolerPower(self):
        '''Returns the power of the cooler in percent'''
        print "Cooler power: {}".format(self.camera.coolerpower)

    
    def setGuiderCCDTemp(self,Temp):
        '''Sets the temperature of the guider camera:
           Temp (float): Temperature in degrees Celsius '''
        self.CCDTemp = Temp
        self.camera.GuiderTemperatureSetpoint = self.CCDTemp
        print "Camera temp setpoint: {} C".format(self.CCDTemp)
    

    def setCoolerStatus(self,status):
        '''Turn on the coolers for both cameras:
           setCoolerStatus('on'): turn coolers on
           setCoolerstatus('off): turn coolers off '''
        if status == 'on':
            self.camera.CoolerOn = True
            print "Cooler on"
        if status == 'off':
            self.camera.CoolerOn = False
            print "Cooler off"

    def setBinning(self,bins):
        '''Sets the binning of the camera:
        setBinning(1): 1x1 binning
        setBinning(2): 2x2 binning
        setBinning(3): 3x3 binning
        setBinning(4): 4x4 binning 
        other values of binning will return error'''
        opt = [1,2,3,4]
        if bins in opt:
            self.camera.BinX = bins
            self.camera.BinY = bins
            print "Camera binning: {}x{}".format(bins,bins)
        else:
            print "ERROR: Invalid bin value"

    def setSubFrame(self,StartX,StartY,NumX,NumY):
        '''Set a subframe for the camera output: speeds up image download.
           StartX : where subframe starts in x axis of image [Pixels]
           StartY : where subframe starts in y axis of image [Pixels]
           NumX : width of subframe [Pixels] mimimum : 16 pixels
           NumY : height of subframe [Pixels]'''
        self.camera.NumX = NumX
        self.camera.NumY = NumY
        self.camera.StartX = StartX
        self.camera.StartY = StartY
        print "Subframe: X[{},{}], Y[{},{}]".format(StartX,StartX+NumX,StartY,StartY+NumY)
    
    def exposeCamera(self,exposure,filter_type = 'luminensce',frame_type = 'light',filename = ''.join([os.getcwd(),'\Image1.fit'])):
        '''Expose the main camera:
        Exposure time: measured in seconds
        filter type: 'luminensce' 
                     'red'
                     'green'
                     'blue'
                     'spectroscope'
                     'Filter6' 

        frame_type: 'light'
                    'dark' 
        filename: include filepath'''
        frame = {'light':1,'dark':0}
        filters = {'luminensce':0,'red':1,'green':2,'blue':3,'spectroscope':4,'Filter6':5}
        print "Exposing..."
        self.camera.Expose(exposure,frame[frame_type],filters[filter_type])
        while not self.camera.ImageReady:
            time.sleep(0.1)
        print "Exposure complete"
        self.camera.SaveImage(filename)
        print "Image saved to: {}".format(filename)

    def exposeGuider(self,exposure,filename = None):
        '''Expose the guide camera:
        Exposure time: measured in seconds
        filename: include filepath (optional)
        '''
        print "Exposing..."
        self.camera.GuiderExpose(exposure)
        print "Exposure complete"
        if filename == None:
            pass
        else:
            self.camera.SaveImage(filename)
            print "Image saved to: {}".format(filename)
        
        
    def guiding(self,exposure):
        ''' Turns on automatic guiding. WARNING: Not fully tested yet'''
        self.camera.GuiderAutoSelectStar = True
        self.xpos = self.camera.GuiderXStarPosition
        self.ypos = self.camera.GuiderYStarPosition
        self.GuiderSetStarPosition(self.xpos,self.ypos)
        try:
            guideStatus = self.camera.GuiderTrack(exposure)
        except:
            print "ERROR: Could not start Guiding"
        if guideStatus:
            print "Guiding..."
        else:
            print "ERROR: Guiding did not start"

    def stopGuiding(self):
        '''Stop the camera from guiding '''
        try:
            self.camera.GuiderStop
        except:
            print "ERROR: Could not stop Guiding"

    def guiderMove(self,direction,time):
        '''Move the telescope manually using the guide camera.
        direction: 0 - positive X direction
                   1 - negative X direction
                   2 - positive Y direction
                   3 - negative Y direction
        time: how long the guider moves the telescope for in seconds'''
        self.camera.guidermove(direction,time)

    def quit(self):
        '''Switch the coolers off and disconnect the camera  '''
        self.setCoolerStatus('off')
        self.camera.LinkEnabled = False
        print "Camera Disconnected"


        
