import win32com.client
import time
import os
import socket
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

def connect():
    ''' Uses Old PC as server: to connect to dome-PC
        Takes no arguments: hostname is this PC's IP or pc name.
        Using IP is quicker than host name'''
    server = SimpleXMLRPCServer((socket.gethostname(),8000),allow_none = True)
    print "hostname: ", socket.gethostname()
    server.register_instance(Camera())
    try:
        print "Ctrl-C to exit"
        server.serve_forever()
    except KeyboardInterrupt:
        print "Exiting"

class Camera:
    def __init__(self):
        '''Connects cameras through MaxImDL through com, use 'object.camera.' to access other MaxImDL functions'''
        self.camera = win32com.client.Dispatch("MaxIm.CCDCamera")
        try:
            self.camera.LinkEnabled = True
            print "Camera connected"
        except:
            print "ERROR: Cannot connect to camera"

    def setCCDTemp(self,strTemp):
        '''Sets the temperature of main camera in degreed Celsius '''
        try: 
            numTemp = float(strTemp)
        except:
            print "ERROR: "
        self.CCDTemp = numTemp
        self.camera.TemperatureSetpoint = self.CCDTemp
        print "Camera temp setpoint: {} C".format(self.CCDTemp)

    
    def setGuiderCCDTemp(self,strTemp):
        '''Sets Guider temperature in degrees Celsius '''
        try: 
            numTemp = float(strTemp)
        except:
            print "Error: "
        self.CCDTemp = numTemp
        self.camera.GuiderTemperatureSetpoint = self.CCDTemp
        print "Camera temp setpoint: {} C".format(self.CCDTemp)

    def coolerPower(self):
        '''Returns the power of the cooler in percent'''
        print "Cooler power: {}".format(self.camera.coolerpower)
    

    def setCoolerStatus(self,status):
        '''Sets the cooler state of all cameras:
          setCoolerStatus('on'): coolers on
          setCoolerStatus('off'): coolers off '''
        if status == 'on':
            self.camera.CoolerOn = True
            print "Cooler on"
        if status == 'off':
            self.camera.CoolerOn = False
            print "Cooler off"

    def setBinning(self,bins):
        '''Sets the binning of the camera:
        setBinning(1): 1x1 binning
        setBinning(2): 2x2 binning
        setBinning(3): 3x3 binning
        setBinning(4): 4x4 binning 
        other values of binning will return error'''
        opt = [1,2,3,4]
        if bins in opt:
            self.camera.BinX = bins
            self.camera.BinY = bins
            print "Camera binning: {}x{}".format(bins,bins)
        else:
            print "ERROR: Invalid bin value"

    def setSubFrame(self,StartX,StartY,NumX,NumY):
        '''Set a subframe for the camera output: speeds up image download.
           StartX : where subframe starts in x axis of image [Pixels]
           StartY : where subframe starts in y axis of image [Pixels]
           NumX : width of subframe [Pixels] mimimum : 16 pixels
           NumY : height of subframe [Pixels]'''
        self.camera.NumX = NumX
        self.camera.NumY = NumY
        self.camera.StartX = StartX
        self.camera.StartY = StartY
        print "Subframe: X[{},{}], Y[{},{}]".format(StartX,StartX+NumX,StartY,StartY+NumY)

    def setFilter(self,filter_type):
        '''Set filter of camera, 
        options : 'luminensce', 'red', 'green', 'blue', 'spectroscope', 'Filter6' '''
        filters = {'luminensce':0,'red':1,'green':2,'blue':3,'spectroscope':4,'Filter6':5}
        self.camera.Filter = filters[filter_type]
        return "Filter set to: ", filter_type

    def exposeCamera(self,exposure,frame_type,filename):
        '''Expose the camera: 
           exposure: exposure time in seconds
           frame_type: 'light' or 'dark' 
           filename: file name including file path '''
        frame = {'light':1,'dark':0}

        print "Exposing..."
        self.camera.Expose(exposure,frame[frame_type])
        while not self.camera.ImageReady:
            time.sleep(0.1)
        print "Exposure complete"
        self.camera.SaveImage(filename)
        print "Image saved to: {}".format(filename)

    def exposeGuider(self,exposure,filename):
        ''' Expose guide camera.
        exposure: Exposure length in seconds
        filename: filename including filepath'''
        print "Exposing..."
        self.camera.GuiderExpose(exposure)
        print "Exposure complete"

    def guiding(self,exposure):
        ''' Turns on automatic guiding. WARNING: Not fully tested yet'''
        self.camera.GuiderAutoSelectStar = True
        self.xpos = self.camera.GuiderXStarPosition
        self.ypos = self.camera.GuiderYStarPosition
        self.GuiderSetStarPositioh(self.xpos,self.ypos)
        try:
            guideStatus = self.camera.GuiderTrack(exposure)
        except:
            print "ERROR: Could not start Guiding"
        if guideStatus:
            print "Guiding..."
        else:
            print "ERROR: Guiding did not start"
    def stopGuiding(self):
        try:
            self.camera.GuiderStop
        except:
            print "ERROR: Could not stop Guiding"

    def quit(self):
        '''Disconnect camera and turn off coolers'''
        self.setCoolerStatus('off')
        self.camera.LinkEnabled = False
        print "Camera Disconnected"



        
