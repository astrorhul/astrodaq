from DMK_VideoCapture import VideoCapture
from MaximDL import Maxim_camera
from Telescope import TelescopeControl
import time
import numpy as np
import matplotlib.pyplot as plt

SBIG_cam = Maxim_camera.Camera()
FireWire_cam = VideoCapture.Camera()
tel = TelescopeControl.Telescope()

def capture():
    image = FireWire_cam.getImage()
    star_pos = np.unravel_index(image.argmax(),image.shape)
    return list(star_pos)

def move(direction,time):
    SBIG_cam.guiderMove(direction,time)

def setGuidePos():
    pos = capture()
    return list(pos)

def exit():
    SBIG_cam.quit()
    FireWire_cam.quit()
    tel.quit()

def guide(guide_pos):
    print guide_pos
    while True:
        pos = capture()
        print pos
        while not pos == guide_pos:
            if pos[0]<guide_pos[0]:
                move(0,1)
                time.sleep(2)
            if pos[0]>guide_pos[0]:
                move(1,1)
                time.sleep(2)
            if pos[1]<guide_pos[1]:
                move(2,1)
                time.sleep(2)
            if pos[1]>guide_pos[1]:
                move(3,1)
                time.sleep(2)
        if pos == guide_pos:
            time.sleep(5)

def startGuide():
    guide_pos = setGuidePos()
    guide(guide_pos)
    


    

        
