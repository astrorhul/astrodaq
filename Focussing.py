import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from Telescope import TelescopeControl as Tel

def Gauss2d((x,y),A,x0,y0,sigx,sigy,rho):
    func = A*(1./(2.*np.pi*sigx*sigy*np.sqrt(1-rho*rho)))*np.exp(-(1./(2.*(1.-rho*rho)))*(((x-x0)*(x-x0)/(sigx*sigx)) + ((y-y0)*(y-y0)/(sigy*sigy)) - 2*rho*(x-x0)*(y-y0)/(sigx*sigy)))
    return func.ravel()

def Data(sigx):
    x = np.linspace(-50,50,100)
    y = np.linspace(-50,50,103)
    lenx = len(x)
    leny = len(y)
    x,y = np.meshgrid(x,y)
    A = 1000
    x0,y0 = 0,0
    sigy = 10
    rho = 0.5
    data = Gauss2d((x,y),A,x0,y0,sigx,sigy,rho)
    noise = np.random.normal(size = data.shape)
    data = data + 0.05*noise
    return np.array(data.reshape(leny,lenx))

def Fit(data):
    #data = Data()
    lenx = len(data[0,:])
    leny = len(data[:,0])
    x,y = np.meshgrid(np.linspace(0,data.shape[0],lenx),np.linspace(0,data.shape[1],leny))
    pos = np.unravel_index(data.argmax(),data.shape)
    print pos
    data = data.ravel()
    param,cov = curve_fit(Gauss2d,(x,y),data,p0 = [1000,pos[0],pos[1],8,9,-0.5])
    fit = Gauss2d((x,y),*param).reshape(leny,lenx)
    return [x,y,data.reshape(leny,lenx),fit,param,lenx,leny]


def Plot():
    data = Fit()
    print data[4]
    plt.imshow(data[2],extent = (data[0].min(),data[0].max(),data[1].max(),data[1].min()),interpolation = 'none')
    plt.contour(data[0],data[1],data[3],colors = 'w')

def Focus():
    data1 = Data(5)
    data2 = Data(10)
    data3 = Data(12)

    w = []
    for i in [data3,data2,data1]:
        fit = Fit(i)
        params = fit[4]
        w.append(params[3]+params[4])
    print w
            
    
    
    



