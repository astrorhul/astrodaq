import win32com.client


class Telescope:
    def __init__(self):
        '''Opens POTH, for connecting to telescope, then connects both the telescope and the focuser.
        reads out current position of telescope in Right ascension (RA) and declination (Dec). All RA and Dec coordinates use decimals not minutes.
        '''
        self.telescope = win32com.client.Dispatch("POTH.Telescope")
        self.focuser = win32com.client.Dispatch("POTH.Focuser")

        try:
            self.telescope.connected = True
            print "Telescope connected"
        except:
            print "Telescope could not connect"
        try:
            self.focuser.link = True
            print "Focuser connected"
        except:
            print "Focuser could not connect"
        if self.telescope.connected:
            print "Current RA (hours): ", self.telescope.rightascension
            print "Current Dec (degrees): ", self.telescope.declination


    def slew(self,RA,Dec):
        '''Slews telescope to right ascension and declination coordinates:
        RA: right ascension in hours (uses decimal not minutes)
        Dec: declination in degrees (uses decimal not minutes)'''
        if not self.telescope.tracking:
            print "Wrong tracking state: Turn on tracking"
        else:
            if Dec<45 and Dec>-45:
                self.telescope.slewtocoordinates(RA,Dec)
            else:
                print "WARNING: Camera will be close to mount"
                print "Are you sure you wan to proceed? y/n"
                x = raw_input("")
                if x == 'y':
                    self.telescope.slewtocoordinates(RA,Dec)
                    if x == 'n':
                        print "Please select new coordinates"
    def Tracking(self,mode):
        '''Sets the tracking mode to on or off:
             Turn tracking on (str): mode = 'on'
             Turn tracking off (str): mode = 'off' '''
        try:
            if mode == 'on':
                self.telescope.Tracking = True
            if mode == 'off':
                self.telescope.Tracking = False
        except:
            if mode == 'on': print "Telescope could not track"
            if mode == 'off': print "Could not stop tracking"


    def telParams(self):
        ''' Returns the Right Ascension(RA) and declination(Dec) of telesocpe
        RA measures in hours (uses decimal not minutes)
        Dec measured in degrees (uses decimal not minutes) '''
        RA = self.telescope.rightascension
        Dec = self.telescope.declination
        print "RA: ", RA
        print "Dec: ", Dec
        return [RA,Dec]

    def moveFocus(self,distance):
        ''' Moves the focuser by relative distance, -ve values compensate so focus is always set with positive value
        distance: relative move ranges from 0 - 7000 unless changed'''
        self.focuser.move(distance)
        print "Position: ", self.focuser.position

    def focusParams(self):
        '''Returns the parameters of the focuser, max step size and maximum position'''
        print "Max step size: ", self.focuser.maxincrement
        print "Max position: ", self.focuser.maxstep


    def quit(self):
        '''Turns off tracking and disconnects telescope and foucser '''
        try:
            self.telescope.Tracking = False
            self.focuser.link = False
            self.telescope.connected = False
        except:
            print "Could not disconnect telescope"
